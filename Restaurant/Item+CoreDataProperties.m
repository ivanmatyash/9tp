//
//  Item+CoreDataProperties.m
//  Restaurant
//
//  Created by admin on 16-05-23.
//  Copyright (c) 2016 admin. All rights reserved.
//

//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Item+CoreDataProperties.h"

@implementation Item (CoreDataProperties)

@dynamic sectionName;
@dynamic itemName;
@dynamic id;
@dynamic price;
@dynamic imageName;
@dynamic itemDescription;

@end
