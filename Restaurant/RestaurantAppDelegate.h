//
//  RestaurantAppDelegate.h
//  Restaurant
//
//  Created by admin on 16-05-23.
//  Copyright (c) 2016 admin. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "SingletonClass.h"
#import <CoreData/CoreData.h>

@interface RestaurantAppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate>{
    
    @private SingletonClass *sharedData;
    
}
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory; // nice to have to reference files for core data


@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UITabBarController *tabBarController;

@end
