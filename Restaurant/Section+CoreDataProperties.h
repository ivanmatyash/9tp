//
//  Section+CoreDataProperties.h
//  Restaurant
//
//  Created by admin on 16-05-23.
//  Copyright (c) 2016 admin. All rights reserved.
//

//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Section.h"

NS_ASSUME_NONNULL_BEGIN

@interface Section (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *sectionName;
@property (nullable, nonatomic, retain) NSString *sectionHeader;
@property (nullable, nonatomic, retain) NSString *sectionFooter;

@end

NS_ASSUME_NONNULL_END
