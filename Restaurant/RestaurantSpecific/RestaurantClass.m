//
//  RestaurantClass.m
//
//  Created by Eric D'Souza on 12-02-23.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RestaurantClass.h"
#import "SingletonClass.h"
#import "LocationViewController.h"


NSString * const kFacebookAppId = @"198087950267379";
NSString * const kAppArchiveDatafile = @"restaurant.dat";

NSString * const kRestaurantName = @"Столовая БГУ";
NSString * const kContactEmail = @"bsu-eat@gmail.com";
NSString * const kTwitterScreenName = @"bsu-canteen";
NSString * const kRestaurantFacebookPage = @"bsu-cateen";

BOOL const kShowOffersTab = TRUE;

NSString * const kOrderTakingCapabilities = @"Расплатится за меню в нашей столовой\nможно только позвонив по телефону.";


@implementation RestaurantClass

@synthesize menuSections, menuItems;
@synthesize restaurants;


+(RestaurantClass*) restaurantDataInstance {
    static RestaurantClass *restaurantDataInstance;
    @synchronized(self) {
        if(!restaurantDataInstance){
            restaurantDataInstance = [[RestaurantClass alloc] init];
        }
    }
    return restaurantDataInstance;
}

-(id) init;
{
    self = [super init];
    if (!self) return nil;
    
    // restaurant locations
    
    CLLocation *restaurant0Location = [[CLLocation alloc] initWithLatitude:53.8938988 longitude:27.5460609];
    NSDictionary *restaurant0 = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 @"8 017 5670914", kPhoneNumber,
                                 restaurant0Location, kLocation,
                                 @"Столовая БГУ", kPointTitle,
                                 @"пр-т Независимости, д. 4", kPointSubtitle,
                                 nil];
    
    self.restaurants = [[NSArray alloc] initWithObjects:
                        restaurant0,
                        nil];
    
    
    // menu sections
    
    NSDictionary *section1 = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @"Горячие блюда", kSectionName,
                              @"Горячие блюда", kSectionHeader,
                              @"", kSectionFooter,
                              nil];
    
    NSDictionary *section2 = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @"Холодные блюда", kSectionName,
                              @"Холодные блюда", kSectionHeader,
                              @"", kSectionFooter,
                              nil];
    
    NSDictionary *section3 = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @"Напитки", kSectionName,
                              @"Напитки", kSectionHeader,
                              @"", kSectionFooter,
                              nil];
    
    NSDictionary *section4 = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @"Выпечка", kSectionName,
                              @"Выпечка", kSectionHeader,
                              @"", kSectionFooter,
                              nil];
    
    NSDictionary *section5 = [[NSDictionary alloc] initWithObjectsAndKeys:
                              @"Десерты", kSectionName,
                              @"Десерты", kSectionHeader,
                              @"", kSectionFooter,
                              nil];
    
    self.menuSections = [[NSArray alloc] initWithObjects:
                         section1,
                         section2,
                         section3,
                         section4,
                         section5,
                         nil];
    
    
    // menu items
    // (formatted and cut-and-pasted from Excel)
    
    
    
    NSDictionary *menuItem1 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Горячие блюда", kSectionName, @"1 Борщ", kItem, @"1", kItemId, @"5000", kPrice, @"1.jpg", kImageName, @"Свежий вкусный борщ", kItemDesc, nil];
    NSDictionary *menuItem2 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Горячие блюда", kSectionName, @"2 Щи", kItem, @"2", kItemId, @"4500", kPrice, @"2.jpg", kImageName, @"Только из свежей капусты", kItemDesc, nil];
    NSDictionary *menuItem3 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Горячие блюда", kSectionName, @"3 Картофельное пюре", kItem, @"3", kItemId, @"4000", kPrice, @"3.jpg", kImageName, @"Будь настоящим беларусом, ешь картошку!", kItemDesc, nil];
    NSDictionary *menuItem4 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Горячие блюда", kSectionName, @"4 Котлета Несвиж", kItem, @"4", kItemId, @"16000", kPrice, @"4.jpg", kImageName, @"Из свежего мяса.", kItemDesc, nil];
    NSDictionary *menuItem5 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Холодные блюда", kSectionName, @"5 Холодник", kItem, @"5", kItemId, @"6000", kPrice, @"5.jpg", kImageName, @"Актуально жарким летом", kItemDesc, nil];
    NSDictionary *menuItem6 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Холодные блюда", kSectionName, @"6 Холодец", kItem, @"6", kItemId, @"8000", kPrice, @"6.jpg", kImageName, @"Холодец с хреном!", kItemDesc, nil];
    NSDictionary *menuItem7 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Холодные блюда", kSectionName, @"7 Окрошка", kItem, @"7", kItemId, @"6000", kPrice, @"7.jpg", kImageName, @"Прекрасно утоляет жажду", kItemDesc, nil];
    NSDictionary *menuItem8 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Холодные блюда", kSectionName, @"8 Солянка", kItem, @"8", kItemId, @"8000", kPrice, @"8.jpg", kImageName, @"Вкуснее вы не пробовали", kItemDesc, nil];
    NSDictionary *menuItem9 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Напитки", kSectionName, @"9 Молоко", kItem, @"9", kItemId, @"3000", kPrice, @"9.jpg", kImageName, @"Пейте дети молоко, будете здоровы!", kItemDesc, nil];
    NSDictionary *menuItem10 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Напитки", kSectionName, @"10 Чай", kItem, @"10", kItemId, @"3000", kPrice, @"10.jpg", kImageName, @"Настоящий цейлонский чай", kItemDesc, nil];
    NSDictionary *menuItem10A = [[NSDictionary alloc] initWithObjectsAndKeys: @"Напитки", kSectionName, @"10A Кофе", kItem, @"10A", kItemId, @"4000", kPrice, @"11.jpg", kImageName, @"Ароматный молотый кофе.", kItemDesc, nil];
    NSDictionary *menuItem11 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Выпечка", kSectionName, @"11 Сметанник", kItem, @"11", kItemId, @"4000", kPrice, @"12.jpg", kImageName, @"Вкус детства!", kItemDesc, nil];
    NSDictionary *menuItem12 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Выпечка", kSectionName, @"12 Пицца", kItem, @"12", kItemId, @"12000", kPrice, @"13.jpg", kImageName, @"Вкусная сытная пицца", kItemDesc, nil];
    NSDictionary *menuItem13 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Выпечка", kSectionName, @"13 Сосиска в тесте", kItem, @"13", kItemId, @"7000", kPrice, @"14.jpg", kImageName, @"Вкусная, очень", kItemDesc, nil];
    NSDictionary *menuItem14 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Десерты", kSectionName, @"14 Мороженное", kItem, @"14", kItemId, @"8000", kPrice, @"15.jpg", kImageName, @"Пломбир", kItemDesc, nil];
    NSDictionary *menuItem15 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Десерты", kSectionName, @"15 Творог", kItem, @"15", kItemId, @"6000", kPrice, @"16.jpg", kImageName, @"Полезно для костей", kItemDesc, nil];
    NSDictionary *menuItem16 = [[NSDictionary alloc] initWithObjectsAndKeys: @"Десерты", kSectionName, @"16 Желе", kItem, @"16", kItemId, @"7000", kPrice, @"17.jpg", kImageName, @"С фруктами", kItemDesc, nil];
    
    
    
    self.menuItems = [[NSArray alloc] initWithObjects:
                      menuItem1,
                      menuItem2,
                      menuItem3,
                      menuItem4,
                      menuItem5,
                      menuItem6,
                      menuItem7,
                      menuItem8,
                      menuItem9,
                      menuItem10,
                      menuItem10A,
                      menuItem11,
                      menuItem12,
                      menuItem13,
                      menuItem14,
                      menuItem15,
                      menuItem16,
                      nil];
    
    return self;
    
}




@end
