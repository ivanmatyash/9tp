//
//  UIColorExtensions.h
//  Restaurant
//
//  Created by admin on 16-05-23.
//  Copyright (c) 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIColor (Extensions)

+ (UIColor *)appleDeleteRedColor;
+ (UIColor *)appleHighlightBlueColor;
+ (UIColor *)appleButtonBlueColor;

+ (UIColor *)myTableBackgroundColor;
+ (UIColor *)tableHeaderColor;
+ (UIColor *)tableFooterColor;
+ (UIColor *)ericdsouzaInfoColor;


@end
