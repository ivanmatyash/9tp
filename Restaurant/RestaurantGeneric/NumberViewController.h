//
//  NumberViewController.h
//  Restaurant
//
//  Created by admin on 16-05-23.
//  Copyright (c) 2016 admin. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol NumberDelegate <NSObject>

- (void)numberViewControllerDidReturnNumber:(int)aNumber;

@end


@interface NumberViewController : UIViewController {
    
    id <NumberDelegate> __unsafe_unretained delegate;
    
    int aNumber;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil currentNumber:(int)currentNumber;

- (IBAction)buttonNumberDidTouchUp:(id)sender;
- (IBAction)buttonDoneDidTouchUp:(id)sender;


@property (nonatomic, unsafe_unretained) id <NumberDelegate> delegate;

@end
