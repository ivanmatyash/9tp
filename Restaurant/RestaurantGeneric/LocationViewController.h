//
//  LocationViewController.h
//  Restaurant
//
//  Created by admin on 16-05-23.
//  Copyright (c) 2016 admin. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface LocationViewController : UIViewController<CLLocationManagerDelegate> {

    CLLocationManager *locationManager;
    CLLocation *restaurantLocation;
    BOOL knowUserLocation;
}

@property (readonly, nonatomic) CLLocation *restaurantLocation;

@property (unsafe_unretained, nonatomic) IBOutlet MKMapView *mapView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *imageMap;

@end
