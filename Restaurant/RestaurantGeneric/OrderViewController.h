//
//  OrderViewController.h
//  Restaurant
//
//  Created by admin on 16-05-23.
//  Copyright (c) 2016 admin. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "SingletonClass.h"
#import "RestaurantClass.h"
#import "MenuItemViewController.h"


@interface OrderViewController : UIViewController <MenuItemDelegate>{
    
    SingletonClass *sharedData;
    RestaurantClass *restaurantData;
    
}

@property (unsafe_unretained, nonatomic) IBOutlet UITableView *tableViewOrderItems;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *labelAboveTableView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *labelNoItems;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *buttonPhone;

- (IBAction)buttonPlaceOrderDidTouchUp:(id)sender; 

@end
