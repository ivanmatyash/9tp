//
//  Section+CoreDataProperties.m
//  Restaurant
//
//  Created by admin on 16-05-23.
//  Copyright (c) 2016 admin. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Section+CoreDataProperties.h"

@implementation Section (CoreDataProperties)

@dynamic sectionName;
@dynamic sectionHeader;
@dynamic sectionFooter;

@end
